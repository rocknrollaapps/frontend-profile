$(function() {

	// Pseudoselect clickers

	$('.contact-form__dropdown > a').on('click', function(e) {
		e.preventDefault();
		if($(this).hasClass('trigger')) {
			$(this).attr('class', 'activetrigger');
			$('.dropdownvisible').show();
		} else {
			$(this).attr('class', 'trigger');
			$('.dropdownvisible').hide();
		}
	});

	$("body").mouseup(function(e) {
		var div = $(".contact-form__dropdown > a"); 
		if (!div.is(e.target) 
		    && div.has(e.target).length === 0) { 
			$('.contact-form__dropdown > a').attr('class', 'trigger');
			$('.dropdownvisible').hide(); 
		}
	});

	// Create slider

	$(".block__slider").slider({
		range: "min",
	    value:3,
	    min: 0,
	    max: 4,
	    step: 1    
    });

    // Menu scrollTo

	$('.header .list__item a').on('click', function(e){
		var link = $(this).attr('class');
		$('html,body').stop().animate({ scrollTop: $('.block.' + link).offset().top }, 1000);
		e.preventDefault();
	});
    
	// Hover on checkboxes

	$('.contact-form__checkbox').hover(
		function(){
		    $(this).next().css('background-image','url(images/hovered.png)');
		},
		function(){
		    $(this).next().css('background-image','url(images/unchecked.png)');
		}
	);

	$('.navbar-toggle').on('click', function() {
		$(this).next().slideToggle();
	});

});

